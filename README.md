# GNU Screen 

My simple configuration [GNU Screen](https://www.gnu.org/software/screen/)

![Print screen](https://pixelfed.social/storage/m/113a3e2124a33b1f5511e531953f5ee48456e0c7/85c26953a179a0e533565ce79e82deb9724813d5/twHS6LMTGdCzL5LHcrM9kwDzeeKA7o3PeX9sQL2r.png)

Install

```
# apt install -y screen cmatrix
```

```
$ cp .screenrc "$HOME"/
```

Start automatically (optional)

Add the snippet at the end of the `"$HOME"/.bashrc` or `"$HOME"/.zshrc` file

```
if [[ -z "$STY" ]]; then
   screen -xRR session_name
fi
```


Create session

```
$ screen -S NAME
```

List sessions

```
$ screen -ls
```

Reattach to a session

```
$ screen -r NAME
```

Shortcuts

<kbd>Ctrl</kbd> + <kbd>a</kbd> + <kbd>c</kbd> create new screen

<kbd>Ctrl</kbd> + <kbd>a</kbd> + <kbd>A</kbd> set title screen

<kbd>Ctrl</kbd> + <kbd>a</kbd> + <kbd>"</kbd> list screens

<kbd>Ctrl</kbd> + <kbd>a</kbd> + <kbd>k</kbd> kill current screen

<kbd>Ctrl</kbd> + <kbd>a</kbd> + <kbd>S</kbd> create footer split

<kbd>Ctrl</kbd> + <kbd>a</kbd> + <kbd>|</kbd> create right split

<kbd>Ctrl</kbd> + <kbd>a</kbd> + <kbd>Tab</kbd> move between split

<kbd>Ctrl</kbd> + <kbd>a</kbd> + <kbd>X</kbd> kill current split

<kbd>Ctrl</kbd> + <kbd>a</kbd> + <kbd>d</kbd> detaches from a session

<kbd>Ctrl</kbd> + <kbd>a</kbd> + <kbd>x</kbd> lock (requires password)

Tested on screen 4.6.2 and Debian 10
